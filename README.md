# Estimation of mechanical stress in heatmap images by a convolutional neural network

Through a convolutional neural network, we analyze images displaying variations in the length and temperature of solid-state materials, and based on this visual information predict their mechanical stress.

##   Abstract
Through a convolutional neural network (CNN), we analyze images displaying variations in the length and temperature of solid-state materials. The images, and their corresponding stress, are generated throughout molecular dynamics in layered alloys upon strain conditions. The CNN is training to predict the stress and Young's modulus. We demonstrate that our CNN has an accuracy of above 95%.

## Introduction

In a recent paper [Comput. Mat. Science 204 (2022) 111147] we calculated some heat transport and mechanical properties of half-Heuslers alloys when they are subject to uni-axial strain at several temperatures.
Here we used molecular dynamics simulations to understand the stress-strain curves and temperature-dependent Young modulus at different temperatures for ZrNiSn, TiNiSn, ZrNiSn, and (Ti, Zr)NiSn alloys.

The molecular dynamics simulation employed a fitted EAM potential, which we obtained throughout quantum mechanical simulations. For instance, simulations were performed in atomic systems like the two snapshots shown below at 300 K. The initial states started at 100 K with four-layers and six-layers, respectively.

We observe in the plotted images of the heatmap that there is a response of the temperature to the strain across the material. With this observation in mind, we set a convolutional neural network to predict the stress base only on the heatmap images. For that, we prepare 2040 labeled images with the value of the computed stresses.
